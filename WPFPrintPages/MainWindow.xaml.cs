﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

namespace WPFPrintPages
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /**
         * @brief 印刷ボタンが押下された時に呼び出されます。
         * 
         * @param [in] sender 印刷ボタン
         * @param [in] e イベント
         */
        private void Print_Click(object sender, RoutedEventArgs e)
        {
            // デフォルトプリンターで印刷します。
            var server = new LocalPrintServer();
            var queue = server.DefaultPrintQueue;

            // 用紙サイズ(A4)と印刷方向(縦)を設定します。
            var ticket = queue.DefaultPrintTicket;
            ticket.PageMediaSize = new PageMediaSize(PageMediaSizeName.ISOA4);
            ticket.PageOrientation = PageOrientation.Portrait;

            // 余白の上と左位置を取得します。
            var area = queue.GetPrintCapabilities().PageImageableArea;
            var originHeight = area.OriginHeight;
            var originWidth = area.OriginWidth;

            // 印刷データを作成します。
            var document = new FixedDocument();

            // サンプルでは3ページとします。
            for (int i = 0; i < 3; ++i)
            {
                // 1ページに描画する内容を設定します。
                var textBlock = new TextBlock();
                textBlock.Text = $"{i + 1}ページ";

                var canvas = new Canvas();
                Canvas.SetTop(textBlock, originHeight);
                Canvas.SetLeft(textBlock, originWidth);
                canvas.Children.Add(textBlock);

                // 1ページのFixedPageは、PageContentでラップしてFixedDocumentに追加します。
                var page = new FixedPage();
                page.Children.Add(canvas);

                var content = new PageContent();
                content.Child = page;

                // 印刷データにページデータを設定します。
                document.Pages.Add(content);
            }

            // 印刷します。
            var writer = PrintQueue.CreateXpsDocumentWriter(queue);
            writer.Write(document, ticket);
        }
    }
}
